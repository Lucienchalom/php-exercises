<?php

/* 7 - Adicione um campo no formulário de adicionar um pet e escreva uma função que receba o número de um CPF e retorne se ele é válido ou não(TRUE or FALSE).
	Basta printar na tela se o número é um cpf válido ou não. */



function verificaCPF($cpf)
{
    $cpf = strval($cpf);


    $valorSomado1 = 0;
    for ($x = 0; $x <= strlen($cpf) - 3; $x++) {
        $valor = $cpf[$x] * (10 - $x);
        $valorSomado1 = $valorSomado1 + $valor;
    }
    $primeiroDigito = $valorSomado1 * 10 % 11;
    if ($primeiroDigito === 10) {
        $primeiroDigito = 0;
    }


    $valorSomado2 = 0;
    for ($x = 0; $x <= strlen($cpf) - 2; $x++) {
        $valor = $cpf[$x] * (11 - $x);
        $valorSomado2 = $valorSomado2 + $valor;
    }
    $segundoDigito = $valorSomado2 * 10 % 11;
    if ($segundoDigito === 10) {
        $segundoDigito = 0;
    }


    if (strlen($cpf) != 11) {
        return 'false';
    } elseif (
        $cpf === '00000000000' ||
        $cpf === '11111111111' ||
        $cpf === '22222222222' ||
        $cpf === '33333333333' ||
        $cpf === '44444444444' ||
        $cpf === '55555555555' ||
        $cpf === '66666666666' ||
        $cpf === '77777777777' ||
        $cpf === '88888888888' ||
        $cpf === '99999999999'
    ) {
        return 'false';
    } elseif ($primeiroDigito != $cpf[9] || $segundoDigito != $cpf[10]) {
        return 'false';
    } else {
        return 'true';
    }
}

echo verificaCPF(444), "\n"; //false pra tamanho
echo verificaCPF(11111111111), "\n"; //true pra tamanho, false pra repetição
echo verificaCPF(12345678901), "\n"; // true pra repetição, false pra digitos
echo verificaCPF(40174577893), "\n"; //true
echo verificaCPF('00418712867'), "\n"; //true 
