<?php
echo 'hello', "\n";
/**
 * Exercicios de PHP basicos
 */

/*
 1 - Escreva uma função que receba um número indeterminado
  de parâmetros(números) e retorne a média desses números.
 */

function media(...$numeros)
{
    $divisor = count($numeros);
    $soma = 0;
    foreach ($numeros as $numero) {
        $soma = $soma + $numero;
    }
    echo '1-', $soma / $divisor, "\n";
}
media(1, 3, 4, 5); //3,25

/* 
2 - Escreva uma função que receba graus Celsius e converta para Fahrenheit.
 */

function celsiusTofahrenheit($celsius)
{
    echo '2-', $celsius * 1. + 32, "\n";
}
celsiusTofahrenheit(24); //56

/* 
3 - Escreva uma função que converta metros para centímetros.
 */

function metrosTocentimetros($metros)
{
    echo '3-', $metros * 100, "\n";
}
metrosTocentimetros(2); //200

/* 
4 - Escreva uma função que receba um número indeterminado
  de parâmetro e imprima o maior e o menor deles.
 */

function maxNmin(...$valores)
{
    echo 'max ', max($valores), "\n", 'min ', min($valores), "\n";
}
maxNmin(1, 3, 4, 5, 3.25, -56, 200); //-56 e 200


/*  
5 - Escreva uma função que receba um número e exiba
  o dia correspondente da semana. (1-Domingo, 2- Segunda, etc.),
   se digitar outro valor deve aparecer valor inválido.
 */

function diaSemana($dia)
{
    switch ($dia) {
    case 1:
        echo 'domingo', "\n";
        break;
    case 2:
        echo 'Segunda', "\n";
        break;
    case 3:
        echo 'Terça', "\n";
        break;
    case 4:
        echo 'Quarta', "\n";
        break;
    case 5:
        echo 'Quinta', "\n";
        break;
    case 6:
        echo 'Sexta', "\n";
        break;
    case 7:
        echo 'Sabado', "\n";
    default:
        echo 'valor de dia inválido', "\n";
    }
}
diaSemana(3); //terça
diaSemana(10); //falso

/* 
6 - Faça um Programa que peça os 3 lados de um triângulo. 
 O programa deverá informar se os valores podem ser um triângulo. 
 Indique, caso os lados formem um triângulo, se o mesmo é:
     equilátero, isósceles ou escaleno.
  - Dicas:
    - Três lados formam um triângulo quando a soma de 
    quaisquer dois lados for maior que o terceiro;
    - Triângulo Equilátero: três lados iguais;
    - Triângulo Isósceles: quaisquer dois lados iguais;
    - Triângulo Escaleno: três lados diferentes;
 */

function testeTriangulo($a, $b, $c)
{
    if ($a + $b > $c && $a + $c > $b && $c + $b > $a) {
        echo 'é um triangulo: ';
        if ($a === $b && $b === $c) {
            echo 'Equilátero', "\n";
        } elseif ($a === $b || $b === $c || $a === $c) {
            echo 'Isóceles', "\n";
        } else {
            echo 'Escaleno', "\n";
        }
    } else {
        echo 'não é um triangulo valido', "\n";
    }
}

testeTriangulo(3, 3, 3); //equilatero
testeTriangulo(2, 3, 4); //escaleno
testeTriangulo(3, 3, 4); //isoceles
testeTriangulo(2, 2, 4); //não é triangulo
